local function centreOut( text, y, tc, noClear )
    term.setCursorPos( ( TERM_X / 2 ) - ( #text / 2 ), y )
    if not noClear then term.clearLine() end

    term.setTextColour( tc or 256 )
    term.write( text )
end

function splash()
    term.setBackgroundColour( 128 )
    term.clear()

    centreOut( "Titanium Evolve", 5, colours.purple )
    centreOut( "Better than OneCode", 6 )

    setLoading( "Installing Titanium", 0 )
end

function setLoading( message, percentage )
    centreOut( message, 12, colours.cyan )
    centreOut( percentage .. "%", 15, 256 )

    term.setCursorPos( ( TERM_X / 2 ) - 15, 14 )
    term.setBackgroundColour( colours.purple )
    term.write( string.rep( " ", ( percentage / 100 ) * 30 ) )

    term.setBackgroundColour( 128 )
end
