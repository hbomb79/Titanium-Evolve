--[[
    Evolve initialise file. Load program states, TML and theme files, run state hooks and show the welcome screen
]]

loading.splash()
loading.setLoading( "Initialising", 10 )

EvolveApp = Evolve()
EvolveApp:loadState "menu"

EvolveApp:importTheme( "master", "Evolve/ui/master.theme" )
EvolveApp:start()
